# Media POC

A test repository to try out the access of media files via vachan-api.

Questions that need answers:
* Is Personal Access token(with Reporter or some other Role) enough to access private or inernal repos?
* Are access rules same in BCS and central gitlab?
* Are access rules same for LFS and regular files?
* Are the auth configuration on vachan enabling and restricting access on git repos as expected, irrespective of repo visibility and other repo specific factors?


## Testing Plan

**Hosts and Repo Visibility**: Different remotes are created for this same repo on [gitlab.bridgeconn.com](https://gitlab.bridgeconn.com/kavitha.raju) and [gitlab.com](https://gitlab.com/bridgeconn) with different visibility levels.

**Personal Access Token and User Roles**: Personal access tokens are being generated from gitlab.bridgeconn.com and gitlab.com for users who have different levels of access on these repos: Owner, Reporter etc

**LFS and File types**: Audio and Video files of following types are added to the repo. 
* mp3
* mp4
* wav
* mov

Out of which mp3 and mp4 are marked as LFS while wav and mov files are kept as normal git files.

**APIs and Permissions on Vachan**: There are two API through which media access is facilitated: [Streaming API](https://api.vachanengine.org/docs#/Media/stream_media_v2_media_gitlab_stream_get) and [Download API](https://api.vachanengine.org/docs#/Media/download_media_v2_media_gitlab_download_get).
On vachan-API first all these repos can be added with "open-access" permission allowing both APIs to provide the requested contents from all repos, irrespective of the repo visibilities.

Later, permissions of these repo can be changed at vachan and tested to see if the API server is blocking un-authorised access. 

## Observations

Personal Access Token --> gitlab.com --> kavitha.raju(owner) --> read_api : glpat-kEdK3W_Wpgcyh353VvSR



<table>
<tr>
	<th rowspan=2>Test No</th>
	<th colspan=2>Vachan-API</th>
	<th colspan=2>Personal Access Token</th>
	<th colspan=2>Repo</th>
	<th colspan=2>Media File</th>
	<th rowspan=2>Accessible</th>
	<th rowspan=2>Remarks</th>
</tr>

<tr>
	<th>API</th><th>Source Permission</th>
	<th>Token Permission</th><th>User Role</th>
	<th>Host</th><th>Visibility</th>
	<th>Type</th><th>LFS?</th>
</tr>

<tr>
	<td>1</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api</td><td>Owner</td>
	<td>Gitlab</td><td>Public</td>
	<td>mp3</td><td>✓ <!--✓ ☓ --></td>
	<td> ✅<!-- ✓❌✅ ☓ --></td><td> </td>
</tr>

<tr>
	<td>2</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api</td><td>Owner</td>
	<td>Gitlab</td><td>Public</td>
	<td>mp4</td><td>✓ <!--✓ ☓ --></td>
	<td> ✅<!-- ✓❌✅ ☓ --></td><td> </td>
</tr>

<tr>
	<td>3</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api</td><td>Owner</td>
	<td>Gitlab</td><td>Public</td>
	<td>wav</td><td>☓ <!--✓ ☓ --></td>
	<td> ✅<!-- ✓❌✅ ☓ --></td><td> </td>
</tr>
<tr>
	<td>4</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api</td><td>Owner</td>
	<td>Gitlab</td><td>Public</td>
	<td>mov</td><td>☓ <!--✓ ☓ --></td>
	<td> ✅<!-- ✓❌✅ ☓ --></td><td> </td>
</tr>


<!-- ---------Gitlab private----------- -->

<tr>
	<td>5</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api</td><td>Owner</td>
	<td>Gitlab</td><td>Private</td>
	<td>mp3</td><td>✓ <!--✓ ☓ --></td>
	<td> ❌<!-- ✓❌✅ ☓ --></td><td> </td>
</tr>

<tr>
	<td>6</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api</td><td>Owner</td>
	<td>Gitlab</td><td>Private</td>
	<td>mp4</td><td>✓ <!--✓ ☓ --></td>
	<td> ❌<!-- ✓❌✅ ☓ --></td><td> </td>
</tr>

<tr>
	<td>7</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api</td><td>Owner</td>
	<td>Gitlab</td><td>Private</td>
	<td>wav</td><td>☓ <!--✓ ☓ --></td>
	<td> ❌<!-- ✓❌✅ ☓ --></td><td> </td>
</tr>
<tr>
	<td>8</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api</td><td>Owner</td>
	<td>Gitlab</td><td>Private</td>
	<td>mov</td><td>☓ <!--✓ ☓ --></td>
	<td> ❌<!-- ✓❌✅ ☓ --></td><td> </td>
</tr>


<!-- -----------BCS-------------------- -->

<tr>
	<td>9</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api</td><td>Owner</td>
	<td>BCS</td><td>Public</td>
	<td>mp3</td><td>✓ <!--✓ ☓ --></td>
	<td>✅ <!-- ✓❌✅ ☓ --></td><td> </td>
</tr>

<tr>
	<td>10</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api</td><td>Owner</td>
	<td>BCS</td><td>Public</td>
	<td>mp4</td><td>✓ <!--✓ ☓ --></td>
	<td>✅ <!-- ✓❌✅ ☓ --></td><td> </td>
</tr>

<tr>
	<td>11</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api</td><td>Owner</td>
	<td>BCS</td><td>Public</td>
	<td>wav</td><td>☓ <!--✓ ☓ --></td>
	<td>✅ <!-- ✓❌✅ ☓ --></td><td> </td>
</tr>
<tr>
	<td>12</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api</td><td>Owner</td>
	<td>BCS</td><td>Public</td>
	<td>mov</td><td>☓ <!--✓ ☓ --></td>
	<td>✅ <!-- ✓❌✅ ☓ --></td><td> </td>
</tr>


<!-- ---------BCS private----------- -->

<tr>
	<td>13</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api</td><td>Owner</td>
	<td>BCS</td><td>Private</td>
	<td>mp3</td><td>✓ <!--✓ ☓ --></td>
	<td>❌ <!-- ✓❌✅ ☓ --></td><td> </td>
</tr>

<tr>
	<td>14</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api</td><td>Owner</td>
	<td>BCS</td><td>Private</td>
	<td>mp4</td><td>✓ <!--✓ ☓ --></td>
	<td>❌ <!-- ✓❌✅ ☓ --></td><td> </td>
</tr>

<tr>
	<td>15</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api</td><td>Owner</td>
	<td>BCS</td><td>Private</td>
	<td>wav</td><td>☓ <!--✓ ☓ --></td>
	<td>❌ <!-- ✓❌✅ ☓ --></td><td> </td>
</tr>
<tr>
	<td>16</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api</td><td>Owner</td>
	<td>BCS</td><td>Private</td>
	<td>mov</td><td>☓ <!--✓ ☓ --></td>
	<td>❌ <!-- ✓❌✅ ☓ --></td><td> </td>
</tr>

<!-- ---------BCS internal----------- -->

<tr>
	<td>17</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api</td><td>Owner</td>
	<td>BCS</td><td>Internal</td>
	<td>mp3</td><td>✓ <!--✓ ☓ --></td>
	<td>❌ <!-- ✓❌✅ ☓ --></td><td> </td>
</tr>

<tr>
	<td>18</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api</td><td>Owner</td>
	<td>BCS</td><td>Internal</td>
	<td>mp4</td><td>✓ <!--✓ ☓ --></td>
	<td>❌ <!-- ✓❌✅ ☓ --></td><td> </td>
</tr>

<tr>
	<td>19</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api</td><td>Owner</td>
	<td>BCS</td><td>Internal</td>
	<td>wav</td><td>☓ <!--✓ ☓ --></td>
	<td>❌ <!-- ✓❌✅ ☓ --></td><td> </td>
</tr>
<tr>
	<td>20</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api</td><td>Owner</td>
	<td>BCS</td><td>Internal</td>
	<td>mov</td><td>☓ <!--✓ ☓ --></td>
	<td>❌ <!-- ✓❌✅ ☓ --></td><td> </td>
</tr>

<!-- --------- Other tokens----------- -->

<tr>
	<td>21</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api + read_repository + read_registory</td><td>Owner</td>
	<td>Gitlab</td><td>Private</td>
	<td>mp3</td><td>✓ <!--✓ ☓ --></td>
	<td>❌ <!-- ✓❌✅ ☓ --></td><td> </td>
</tr>

<tr>
	<td>22</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api + read_repository + read_registory</td><td>Owner</td>
	<td>Gitlab</td><td>Private</td>
	<td>mp4</td><td>✓ <!--✓ ☓ --></td>
	<td>❌ <!-- ✓❌✅ ☓ --></td><td> </td>
</tr>

<tr>
	<td>23</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api + read_repository + read_registory</td><td>Owner</td>
	<td>Gitlab</td><td>Private</td>
	<td>wav</td><td>☓ <!--✓ ☓ --></td>
	<td>❌ <!-- ✓❌✅ ☓ --></td><td> </td>
</tr>
<tr>
	<td>24</td>
	<td>Stream</td><td>Open-access</td>
	<td>read_api + read_repository + read_registory</td><td>Owner</td>
	<td>Gitlab</td><td>Private</td>
	<td>mov</td><td>☓ <!--✓ ☓ --></td>
	<td>❌ <!-- ✓❌✅ ☓ --></td><td> </td>
</tr>

<tr>
	<td>25</td>
	<td>Stream</td><td>Open-access</td>
	<td>api (all read and write permissions)</td><td>Owner</td>
	<td>Gitlab</td><td>Private</td>
	<td>mp3</td><td>✓ <!--✓ ☓ --></td>
	<td>❌ <!-- ✓❌✅ ☓ --></td><td> </td>
</tr>

<tr>
	<td>26</td>
	<td>Stream</td><td>Open-access</td>
	<td>api (all read and write permissions)</td><td>Owner</td>
	<td>Gitlab</td><td>Private</td>
	<td>mp4</td><td>✓ <!--✓ ☓ --></td>
	<td>❌ <!-- ✓❌✅ ☓ --></td><td> </td>
</tr>

<tr>
	<td>27</td>
	<td>Stream</td><td>Open-access</td>
	<td>api (all read and write permissions)</td><td>Owner</td>
	<td>Gitlab</td><td>Private</td>
	<td>wav</td><td>☓ <!--✓ ☓ --></td>
	<td>❌ <!-- ✓❌✅ ☓ --></td><td> </td>
</tr>
<tr>
	<td>28</td>
	<td>Stream</td><td>Open-access</td>
	<td>api (all read and write permissions)</td><td>Owner</td>
	<td>Gitlab</td><td>Private</td>
	<td>mov</td><td>☓ <!--✓ ☓ --></td>
	<td>❌ <!-- ✓❌✅ ☓ --></td><td> </td>
</tr>

</table>


## Conclusion

Irrespective of host, file types and whether LFS or not, **only public** repos are being accessible from vachan-api.

_NOTE_ : In earlier tests it was seen that only the `http_get` method provided by gitlab would work for LFS files and not the `files` method which is the "proper way". Haven't tested using the `files` method now for the non-lfs files, as our usecase is for LFS files and we have other reasons too to rethink our current design of gitlab media access.